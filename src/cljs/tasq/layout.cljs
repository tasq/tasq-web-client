(ns tasq.layout
  (:require
    [om.core :as om]
    [om-tools.core :refer-macros [defcomponent]]
    [sablono.core :refer-macros [html]]
    [tasq.components.add-task :refer [add-task]]
    [tasq.pages.task :refer [task-list-page]]
    [tasq.components.menu :refer [menu]]
    [tasq.components.login :refer [login]]))

(defmulti render-page :active-page)

(defmethod render-page :task-list [application-state]
  (om/build task-list-page application-state))

(defcomponent tasq-root [state _]
  (render [_]
    (html [:div.application-container
           (case (:authenticated state)
             true (html
                    (om/build menu state)
                    [:div.application-main
                     (render-page state)])
             (html
               (om/build login state)))])))
