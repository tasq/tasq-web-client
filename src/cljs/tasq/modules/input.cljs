(ns tasq.modules.input
  (:require
    [om.core :as om]))

(defn handle-change [e owner keys]
  (om/set-state! owner keys (.. e -target -value)))

(defn text
  ([owner keys]
   (text {} owner keys))
  ([attributes owner keys]
   [:input (merge {:type      "text"
                   :value     (om/get-state owner keys)
                   :on-change #(handle-change % owner keys)}
             attributes)]))
