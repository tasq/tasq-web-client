(ns tasq.utils)

(defn js->clj-kw
  "js->cljで、キーはキーワード化するようにしたやつ。"
  [ds]
  (js->clj ds :keywordize-keys true))
