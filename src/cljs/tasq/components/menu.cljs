(ns tasq.components.menu
  (:require
    [om.core :as om]
    [om-tools.core :refer-macros [defcomponent]]
    [sablono.core :refer-macros [html]]
    [tasq.pages.task :refer [task-list-page]]
    [tasq.components.add-task :refer [add-task]]))

(defn menu-sub-item [state key title count]
  [:li.menu-sub-item
   (if (= key (:selected-menu state)) {:class "selected"})
   [:a {:href (str "#/" key) :on-click #(om/update! state :selected-menu key)} title (if (> count 0) [:span.task-count count])]]
   )

(defcomponent menu [state _]
  (render-state [_ _]
    (html [:section.navigation {:role "banner"}
           [:div.logo
            [:a.logo-link {:href "/"}
             [:img.logo-img {:src "https://raw.githubusercontent.com/thoughtbot/refills/master/source/images/placeholder_logo_1.png"
                             :alt "Tasq"}]]]
           [:div.add-task
            [:button.add-task-button
             {:on-click #(om/update! state :adding-task true)}
             [:i.fa.fa-plus]
             "Add Task"]
            (if (:adding-task state)
              (om/build add-task state))]
           [:nav.menu {:role "navigation"}
            [:h2.menu-title "Menu"]
            [:section.menu-item
             [:h3.menu-item-title [:i.fa.fa-user] "Personal"]
             [:ol.menu-sub-items
              (menu-sub-item state :personal/today "Today" 2)
              (menu-sub-item state :personal/iteration "Iteration" 8888)]]
            [:section.menu-item
             [:h3.menu-item-title [:i.fa.fa-users] "Team"]
             [:ol.menu-sub-items
              (menu-sub-item state :team/iteration "Iteration" 9999)
              (menu-sub-item state :team/graph "Graph" 0)]]
            [:section.menu-item
             [:h3.menu-item-title [:i.fa.fa-line-chart] "Chart"]
             [:ol.menu-sub-items
              (menu-sub-item state :chart/burndown "Burndown" 0)
              (menu-sub-item state :chart/gantt "Gantt" 0)]]]
           [:div.others
            [:a.other
             {:href "#/configuration"
              :on-click #(om/update! state :selected-menu :configuration)}
             [:i.fa.fa-cog]]
            [:a.other
             {:href "#/logout"
              :on-click #(om/update! state :authenticated false)}
             [:i.fa.fa-sign-out]]]])))
