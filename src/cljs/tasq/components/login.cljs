(ns tasq.components.login
  (:require
    [om.core :as om]
    [om-tools.core :refer-macros [defcomponent]]
    [sablono.core :refer-macros [html]]
    [tasq.pages.task :refer [task-list-page]]
    [tasq.components.add-task :refer [add-task]]))

(defcomponent login [state _]
  (render [_]
    (html [:div.login
           [:input.login-id {:type "text"}]
           [:input.password {:type "text"}]
           [:button
            {:on-click #(om/update! state :authenticated true)}
            "LOGIN"]])))
