(ns tasq.components.add-task
  (:require
    [om.core :as om]
    [om-tools.core :refer-macros [defcomponent]]
    [sablono.core :refer-macros [html]]
    [tasq.api :as api]
    [tasq.modules.input :as input]))

(defcomponent add-task [app-state owner]
  (init-state [_]
    {:adding {:title "" :description ""}})
  (render-state [_ state]
    (html
      [:div
       [:div.modal-overlay
        {:on-click #(om/update! app-state :adding-task false)}]
       [:div.modal-body
        [:div.modal-heading
         [:h2 "Create Task"]]
        [:div.modal-content
         [:div.form-container.pure-form.pure-form-aligned
          [:fieldset
           [:div.pure-control-group
            [:label {:for "add-task-title"} "Title"]
            (input/text {:id "add-task-title" :class "pure-u-3-4"} owner [:adding :title])]
           [:div.pure-control-group
            [:label {:for "add-task-description"} "Description"]
            (input/text {:id "add-task-description" :class "pure-u-3-4"} owner [:adding :description])]]]
         [:div.buttons-container
          [:div.buttons
           [:button.pure-button.pure-button-primary {:on-click #(api/request "/task" :POST (:adding state)
                                                      {:handler (fn [_]
                                                                  (api/request "/task/list"
                                                                    {:handler (fn [res] (om/update! app-state :tasks res))})
                                                                  (om/set-state! owner :adding {:title "" :description ""})
                                                                  (om/update! app-state :adding-task false))})} "Add"]
           [:a
            {:on-click #(om/update! app-state :adding-task false)}
            "Cancel"]]]]]])))
