(ns tasq.core
  (:require
    [om.core :as om]
    [tasq.layout :refer [tasq-root]]
    [tasq.api]
    [tasq.pages.task]))

(enable-console-print!)

(defonce tasq-app-state
  (atom
    {:app-name      "Tasq"
     :authenticated false
     :active-page   :task-list
     :adding-task   false
     :selected-menu :personal/today}))

(om/root tasq-root tasq-app-state
  {:target (.getElementById js/document "app-wrapper")})
