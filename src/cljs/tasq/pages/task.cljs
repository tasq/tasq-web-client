(ns tasq.pages.task
  (:require
    [om.core :as om]
    [om-tools.core :refer-macros [defcomponent]]
    [sablono.core :refer-macros [html]]
    [tasq.api :as api]
    ))

(defcomponent task-list-page [app-state _]
  (will-mount [_]
    (api/request "/task/list"
                 {:handler (fn [res]
                             (om/update! app-state :tasks res))}))
  (render [_]
    (html
      [:section.task-list
       [:h3.title "Today"]
       (for [task (:tasks app-state)]
         [:div.task-item
          {:on-click #(println "task" (:taskId task) "is selected")
           :key      (:taskId task)}
          [:h4.task-subject (:title task)]
          [:div.task-properties
           [:span.task-assignee [:i.fa.fa-user] (:assignee task)]
           [:span.task-limit "2015/07/23"]
           ]
          [:p.task-desc (:description task)]]
         )
       ]
      )))
