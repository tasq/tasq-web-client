Tasq Web Client Style Guide
===============================

Tasq Web Client のスタイルガイド。

Design Guideline
-------------------------------

[Eight Times UI Guideline](http://ameblo.jp/ca-1pixel/day-20140507.html)がなんかわかりやすそう。

### Definitions
1. 余白やブロックサイズは、8の倍数で
2. PCサイトの横幅は、1280pxで
3. 隣り合う比率は、黄金比とフィボナッチ数列で（8, 16, 24, 40, 64, 120, 184, 304, 488, 792, 1280）
4. バナーガイドラインを考慮
5. 最小余白は16pxで

### Exception rules
1. 8の倍数で数値が合わない場合は、4の倍数を使っていい。
2. 罫線の1pxの含め方は都合よく解釈していい。
3. 8の倍数、4の倍数でも適応が難しい場合は余白ルールを大きく崩さない程度に1px単位で調整していい。
4. 最終的には、自分のセンスで判断する。

Modularization Guideline
-------------------------------

SMACCSとかOOCSSとかMVCSSとかいろいろと良いやり方があるんだろうけど、
まだ勉強できてないし、試しながらやってみることにする。

### Markup policy

全体のレイアウトとかに関しては、タグに対してスタイルは当てない。絶対に。
タグに対してスタイルを当てていいのは、Coreのファイルだけ。

### Core

全部の画面で共通で利用する、画面レイアウト全体に関わるものを定義する。

スタイルのリセットとタイポグラフィを除いて、HTMLタグに対するスタイル定義はしない。

`_reset.scss`
: スタイルのリセット。 [sanitize.css](http://jonathantneal.github.io/sanitize.css/) をベースにしてる。

`_settings.scss`
: Scssの設定値。色の定義とか、フォントや文字サイズとか。

`_helpers.scss`
: Scssのヘルパー関数とか。Mix-inとか、Placeholder selectorとかそういうのも。 [Bourbon](http://bourbon.io) 使う。

`_base.scss`
: アプリケーションのタイポグラフィ(フォントとか、色とか)。

`_content.scss`
: 要素のマージンとかパディングとか。

`_grid.scss`
: グリッドシステム。 [Neat](http://neat.bourbon.io) をベースにしてる。


### Modules

個々の画面で直接使う部品を定義する。

基本的には、グリッド定義とタイポグラフィ定義をextendして部品を構成する。

`_buttons.scss`
: ボタン

`_modal.scss`
: モーダル画面

`_tooltip.scss`
: ツールチップ


### Components

`_task-list.scss`
: タスクリスト

### Application

`_main.scss`
: ページのメイン領域のレイアウトを設定する。

`_menu.scss`
: ページのメニュー領域のレイアウトを設定する。

`tasq.scss`
: アプリケーションで使うCSSを読み込む。INBOXに実験的なCSS書いたりする。
