@charset "utf-8";

@import "../../vendor/bourbon/app/assets/stylesheets/bourbon";

/*
Settings

変数値とかをまとめておくファイル。

* @font-faceでのフォント定義
* 変数

は、ひとつのファイルにまとめといたほうが見通しがいいって。

Weight: -50

Style Guide: core.settings
*/

/*
$text-color

テキストの標準色は、`$text-color`を使用する。

Style Guide: core.settings.color
*/
$text-color: rgb(66, 66, 66);

/*
$base-color

背景色が必要な場合は、`$base-color`を使う。

Style Guide: core.settings.color
*/
$base-color: transparentize(rgb(255, 255, 255), 1);

/*
$main-color

ヘッダやロゴ色、リンク、見出しとか。全体の20%くらいになるといい。

Style Guide: core.settings.color
*/
$main-color: rgb(71, 151, 195);

$light-heading-background: rgb(253, 252, 249);
$light-heading-color: rgb(66, 66, 66);
$dark-heading-background: rgba(243, 238, 224, .3);
$dark-heading-color: rgb(170, 166, 156);

$icon-label-font-color: rgb(238, 238, 238);

$light-border: darken($light-heading-background, 5) 1px solid;
$dark-border: darken($light-heading-background, 10) 1px solid;

/*
$accent-color

重要なボタンとか。全体の5%くらいになるといい。

Style Guide: core.settings.color
*/
$accent-color: rgb(71, 151, 195);
$alert-color: rgb(240, 95, 92);
$success-color: rgb(168, 186, 110);
$warning-color: rgb(163, 135, 42);
$info-color: rgb(75, 42, 163);

/*
Basic Font Colors

基本的なフォントカラーの定義。

.main-color     - リンクとかに。全体の20%くらいがいいみたい。
.accent-color   - 何か目立たせたいところに。全体の5%くらいがいいみたい。
.alert-color    - エラーの通知に。
.success-color  - 成功したよ系の通知に。
.warning-color  - ん〜これはよくないなぁ系の通知に。
.info-color     - 備考的なやつに。

Markup:
<div class="example-box-basic-layout {{modifier_class}}">サンプル文字列</div>
<div class="example-box-invert-layout {{modifier_class}}">サンプル文字列</div>

Weight: -10

Style Guide: core.settings.color
*/
.example-box-basic-layout {
  width: 80%;
  height: 30px;
  margin: auto;
  text-align: center;
  border: 1px solid #eee;
  background: $base-color;

  &.main-color {
    color: $main-color;
  }

  &.accent-color {
    color: $accent-color;
  }

  &.alert-color {
    color: $alert-color;
  }

  &.success-color {
    color: $success-color;
  }

  &.warning-color {
    color: $warning-color;
  }

  &.info-color {
    color: $info-color;
  }
}

.example-box-invert-layout {
  width: 80%;
  height: 30px;
  margin: auto;
  text-align: center;
  border: 1px solid #eee;
  color: #eee;

  &.main-color {
    background: $main-color;
  }

  &.accent-color {
    background: $accent-color;
  }

  &.alert-color {
    background: $alert-color;
  }

  &.success-color {
    background: $success-color;
  }

  &.warning-color {
    background: $warning-color;
  }

  &.info-color {
    background: $info-color;
  }
}

/*
Basic Fonts

基本的なフォント設定。

`$base-font-family`
: 本文とかのプロポーショナルフォント。

`$base-font-size`
: 基本的なフォントサイズ。他のフォントサイズは、これをベースに計算する。

`$base-line-height`
: なんか、行の高さ的なあれ。

`$mono-font-family`
: 等幅フォント。コードとかの表示に使う。

Style Guide: core.settings.fonts
*/
$modular-scale-base: 1rem;
$modular-scale-ratio: $minor-third;

$base-font-family: 'Lucida Grande', 'Helvetica', 'Hiragino Kaku Gothic ProN', 'ヒラギノ角ゴ ProN W3', 'YuGothic', '游ゴシック', 'Meiryo', 'メイリオ', sans-serif;
$base-font-size: 16px;
$base-line-height: modular-scale(2);
$mono-font-family: 'Monaco', 'Menlo', 'Consolas', monospace;

$Small-font-size: modular-scale(-2);
$small-font-size: modular-scale(-1);
$large-font-size: modular-scale(1);
$Large-font-size: modular-scale(2);
$LARGE-font-size: modular-scale(3);
$huge-font-size: modular-scale(4);
$Huge-font-size: modular-scale(5);
