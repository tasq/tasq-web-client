(defproject tasq-web-client "0.1.0-SNAPSHOT"
  :description "Web browser client for Tasq."
  :url "http://wiki.itr0.pw/display/TASQ/Tasq"
  :license {:name "Apache License, Version 2.0"
            :url  "http://www.apache.org/licenses/LICENSE-2.0.txt"}

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.222"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.omcljs/om "0.9.0"]
                 [prismatic/om-tools "0.4.0"]
                 [om-sync "0.1.1"]
                 [sablono "0.3.6"]
                 [secretary "2.0.0.1-260a59"]]

  :plugins [[lein-cljsbuild "1.1.0"]]

  :source-paths ["src/cljs"]
  :resource-paths ["resources"]

  :hooks [leiningen.cljsbuild]

  :cljsbuild {:builds
              {:client
               {:source-paths ["src/cljs"]
                :compiler     {:output-to     "resources/public/js/tasq.min.js"
                               :main          tasq.core
                               :optimizations :advanced
                               :pretty-print  false}}}}

  :profiles {:dev {:dependencies [[figwheel "0.4.0"]]
                   :plugins      [[lein-figwheel "0.4.0"]]
                   :source-paths ["dev-src/cljs"]
                   :cljsbuild    {:builds
                                  {:figwheel
                                   {:source-paths ["src/cljs" "dev-src/cljs"]
                                    :compiler     {:output-to            "resources/public/js/tasq.min.js"
                                                   :output-dir           "resources/public/js/out"
                                                   :main                 tasq.dev
                                                   :optimizations        :none
                                                   :asset-path           "js/out"
                                                   :source-map           "resources/public/js/tasq.min.js.map"
                                                   :source-map-timestamp true
                                                   :pretty-print         true
                                                   :cache-analysis       true}}}}
                   :figwheel     {:http-server-root "public" ;; default and assumes "resources"
                                  :server-port      3449    ;; default
                                  :css-dirs         ["resources/public/css"] ;; watch and update CSS

                                  ;; Start an nREPL server into the running figwheel process
                                  ;; :nrepl-port 7888

                                  ;; Server Ring Handler (optional)
                                  ;; if you want to embed a ring handler into the figwheel http-kit
                                  ;; server, this is simple ring servers, if this
                                  ;; doesn't work for you just run your own server :)
                                  ;; :ring-handler hello_world.server/handler

                                  ;; To be able to open files in your editor from the heads up display
                                  ;; you will need to put a script on your path.
                                  ;; that script will have to take a file path and a line number
                                  ;; ie. in  ~/bin/myfile-opener
                                  ;; #! /bin/sh
                                  ;; emacsclient -n +$2 $1
                                  ;;
                                  ;; :open-file-command "myfile-opener"

                                  ;; if you want to disable the REPL
                                  ;; :repl false

                                  ;; to configure a different figwheel logfile path
                                  :server-logfile   "tmp/figwheel-logfile.log"
                                  }}}

  :clean-targets ^{:protect false} [:target-path
                                    "resources/public/js/out"
                                    "resources/public/js/tasq.min.js"]
  )
