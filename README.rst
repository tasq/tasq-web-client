========================
Task Web Client
========================

TasqのWebクライアントアプリケーション。

------------------------
Node.jsのインストール
------------------------

`anyenv <https://github.com/riywo/anyenv>`_ をインストールする。

.. code-block:: bash

   git clone https://github.com/riywo/anyenv ~/.anyenv
   echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> ~/.zshrc
   echo 'eval "$(anyenv init -)"' >> ~/.zshrc
   exec $SHELL -l

`ndenv <https://github.com/riywo/ndenv>`_ をインストールする。

.. code-block:: bash

   anyenv install ndenv

Node.jsをインストールする。

.. code-block:: bash

   ndenv install `cat .node-version`

依存ライブラリをインストールする。

.. code-block:: bash

   npm install


------------------------
Scssのビルド
------------------------

.. code-block::

   npm run sass:min

もしくは、変更を監視するなら

.. code-block::

   npm run sass:watch

------------------------
Leiningenのインストール
------------------------

.. code-block::

   brew install leiningen

------------------------
アプリの起動
------------------------

.. code-block::

   lein figwheel



==========================
IntelliJ IDEA 14.1 の設定
==========================

------------------------
Cursiveのインストール
------------------------

ClojureScriptで開発するので、 `Cursive <https://cursiveclojure.com/>`_ をインストールする。

Plugins > Browse Repositories > Manage Repositories で、CursiveのプラグインリポジトリのURLを設定する。
`Getting Started <https://cursiveclojure.com/userguide/>`_ を参照。

* 14.1の場合は、 ``https://cursiveclojure.com/plugins-14.1.xml``.
* 15の場合は、 ``https://cursiveclojure.com/plugins-15.xml``.

プラグインの一覧に ``cursive-xx.xx`` みたいなのが出てくるようになるから、インストールする。

------------------------
プロジェクトとして取込
------------------------

File > New > Project From Existing Sources で、 ``project.clj`` を選択する。