(ns tasq.dev
  (:require
    [tasq.core]
    [figwheel.client :as fw]))

(println "Running in development mode!")

(fw/start {:websocket-url "ws://localhost:3449/figwheel-ws"
           :on-jsload     (fn []
                            ;; (stop-and-start-my app)
                            )})
